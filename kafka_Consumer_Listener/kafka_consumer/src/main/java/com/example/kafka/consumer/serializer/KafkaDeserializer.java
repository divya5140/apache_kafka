package com.example.kafka.consumer.serializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.kafka.consumer.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaDeserializer implements Deserializer<MessageDto> {

	@Override
	public MessageDto deserialize(String topic, byte[] data) {

		ObjectMapper mapper = new ObjectMapper();
		if (Objects.isNull(data)) {
			log.error("Null value received during deserialization");

			return null;
		} else {
			try {
				return mapper.readValue(data, MessageDto.class);
			} catch (IOException e) {
				log.error("Error occured during Deserializing");
				throw new SerializationException("error occured during Deserializing ");
			}
		}
	}

}
