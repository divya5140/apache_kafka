package com.example.kafka.consumer.serializer.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

import com.example.kafka.consumer.dto.MessageDto;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageReader {
	
	
	@KafkaListener(topicPartitions = @TopicPartition(topic="consumer-listener",partitionOffsets =
		{@PartitionOffset(partition = "0",initialOffset = "0")}),containerFactory = "concurrentKafkaListenerContainerFactory")
	public void consumeFromPartitionZero(MessageDto messageDto)
	{
		log.info("Message received: {} from partition 0",messageDto.getContactNo());
	}
	
	
	@KafkaListener(topicPartitions = @TopicPartition(topic="consumer-listener",partitionOffsets =
		{@PartitionOffset(partition = "1",initialOffset = "1")}),containerFactory = "concurrentKafkaListenerContainerFactory")
	public void consumeFromPartitionOne(MessageDto messageDto)
	{
		log.info("Message received: {} from partition 1",messageDto.getContactNo());
	}

}
