package com.example.kafka.windowing.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.windowing.service.WindowsOperation;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class WindowsOperationController {

	private final WindowsOperation windowOperations;

	@GetMapping("/hopping")
	public void widowHoppingOperation() {
		windowOperations.executeHoppingWindowAggregation();
	}

	@GetMapping("/tumbling")
	public void widowTumblingOperation() {
		windowOperations.executeTumblingWindowAggregation();
	}

	@GetMapping("/session")
	public void widowSessionOperation() {
		windowOperations.executeSessionWindowOperation();
	}
}
