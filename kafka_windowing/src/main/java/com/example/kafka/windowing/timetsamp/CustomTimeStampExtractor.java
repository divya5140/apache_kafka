package com.example.kafka.windowing.timetsamp;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.streams.processor.TimestampExtractor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomTimeStampExtractor implements TimestampExtractor{

	@Override
	public long extract(ConsumerRecord<Object, Object> record, long partitionTime) {
		long timestamp=System.currentTimeMillis();
		log.info("Extracting event timestamp:{}",timestamp);
		return timestamp;
	}

}
