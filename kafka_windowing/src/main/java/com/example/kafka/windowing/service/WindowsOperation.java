package com.example.kafka.windowing.service;

import java.time.Duration;
import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.Suppressed;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.springframework.stereotype.Service;

import com.example.kafka.windowing.timetsamp.CustomTimeStampExtractor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WindowsOperation {

	private static Properties windowsOperations() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "windows-operations");
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		return properties;
	}

	public void executeHoppingWindowAggregation() {
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> inputStream = builder
				.stream("hopping-window", Consumed.with(Serdes.String(), Serdes.String())
				.withTimestampExtractor(new CustomTimeStampExtractor()))
				.peek((key, value) -> log.info("Incoming stream key: {} and value: {}", key, value));
		Duration windowSize = Duration.ofSeconds(10);
		Duration advanceSize = Duration.ofSeconds(5);
		inputStream.groupByKey().windowedBy(TimeWindows.ofSizeWithNoGrace(windowSize).advanceBy(advanceSize)).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("windowed key : {} and count: {}", key, value));
		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), windowsOperations());
		kafkaStreams.start();
	}

	public void executeTumblingWindowAggregation() {
		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> inputStream = builder
				.stream("tumbling-window", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Incoming stream key: {} and value: {}", key, value));

		inputStream.groupByKey().windowedBy(TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(20))).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("windowed key : {} and count: {}", key.key(), value));
		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), windowsOperations());
		kafkaStreams.start();
	}

	public void executeSessionWindowOperation() {

		StreamsBuilder builder = new StreamsBuilder();

		KStream<String, String> inputStream = builder
				.stream("session-windows", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Incoming key:{} and value:{}", key, value));

		inputStream.groupByKey().windowedBy(SessionWindows.ofInactivityGapWithNoGrace(Duration.ofSeconds(10))).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("Windowed key:{} and count:{}", key, value));

		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), windowsOperations());
		kafkaStreams.start();
	}

}
