package com.example.kafka.consumer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.consumer.service.MessageReader;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {

	private final MessageReader messageReader;
	
	@GetMapping("/consume")
	public void consumeMessage()
	{
		messageReader.runConsumer();
	}
}
