package com.example.kafka.streams.service;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.stereotype.Service;

import com.example.kafka.streams.configuration.StreamProperties;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class BasicOperations {

	private final StreamProperties properties;

	public void performOperations() {

		StreamsBuilder builder = new StreamsBuilder();
		builder.stream("kafka-stream-input", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Source processor key: {} and value: {}", key, value))
				.filter((key, value) -> value.length() >= 4)
				.peek((key, value) -> log.info("Sink processor key: {} and value: {}", key, value))
				.to("kafka-stream-output", Produced.with(Serdes.String(), Serdes.String()));
		;

		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), properties.streamProperties());
		kafkaStreams.start();

	}


}
