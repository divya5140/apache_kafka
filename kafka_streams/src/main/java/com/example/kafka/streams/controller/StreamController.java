package com.example.kafka.streams.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.streams.service.BasicOperations;
import com.example.kafka.streams.service.JoinOperations;
import com.example.kafka.streams.service.KtableOperations;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class StreamController {

	private final BasicOperations basicOperations;

	private final JoinOperations joinOperations;
	
	private final KtableOperations ktableOperations;

	@GetMapping("/streams")
	public void executeStreams() {
		basicOperations.performOperations();
	}

	@GetMapping("/stream-joins")
	public void exceuteStreamsJoin() {
		joinOperations.StreamJoins();
	}

	@GetMapping("/streams-table-joins")
	public void executeStreamsTableJoin() {
		joinOperations.StreamTableJoins();
	}
	
	@GetMapping("/table")
	public void executeTable()
	{
		ktableOperations.ktableoperations();
	}
}



