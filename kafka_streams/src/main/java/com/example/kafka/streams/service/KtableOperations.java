package com.example.kafka.streams.service;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.stereotype.Service;

import com.example.kafka.streams.configuration.StreamProperties;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class KtableOperations {

	private final StreamProperties properties;
	
	public void ktableoperations() {
		
		StreamsBuilder streamsBuilder=new StreamsBuilder();
		streamsBuilder.table("kafka-table-input",Materialized.<String,String,KeyValueStore<Bytes, byte[]>>as("ktable-store")
				.withKeySerde(Serdes.String()).withValueSerde(Serdes.String()))
		.filter((key,value) -> value.length()>=6)
		.toStream().peek((key,value) -> log.info("Source key : {} and value : {}",key,value))
		.to("kafka-table-output",Produced.with(Serdes.String(),Serdes.String()));
		
		KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties.streamProperties());
		kafkaStreams.start();
	}

}
