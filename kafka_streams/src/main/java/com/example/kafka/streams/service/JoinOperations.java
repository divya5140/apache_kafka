package com.example.kafka.streams.service;

import java.time.Duration;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.StreamJoined;
import org.apache.kafka.streams.kstream.ValueJoiner;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.stereotype.Service;

import com.example.kafka.streams.configuration.StreamProperties;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class JoinOperations {

	private final StreamProperties properties;

	public void StreamJoins() {
		StreamsBuilder streamsBuilder = new StreamsBuilder();
		KStream<String, String> leftStream = streamsBuilder.stream("left-stream",
				Consumed.with(Serdes.String(), Serdes.String()));
		leftStream.peek((key, value) -> log.info("Left stream incoming key is {} and value is {}", key, value));
		KStream<String, String> rightStream = streamsBuilder.stream("right-stream",
				Consumed.with(Serdes.String(), Serdes.String()));
		rightStream.peek((key, value) -> log.info("Right stream incoming key is {} and value is {}", key, value));
		
		ValueJoiner<String, String, String> messageJoiner = (leftMessage, rightMessage) -> {
			return leftMessage + "is joined with " + rightMessage;
		};
		leftStream
				.join(rightStream, messageJoiner,
						JoinWindows.ofTimeDifferenceAndGrace(Duration.ofSeconds(30), Duration.ofHours(24)),
						StreamJoined.with(Serdes.String(), Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("joined key :{} and value :{}", key, value));
		KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties.streamProperties());
		kafkaStreams.start();

	}

	public void StreamTableJoins() {
		StreamsBuilder streamsBuilder = new StreamsBuilder();
		KStream<String, String> leftStream = streamsBuilder.stream("left-stream",
				Consumed.with(Serdes.String(), Serdes.String()));
		leftStream.peek((key, value) -> log.info("Left Stream incoming key is {} and value is {}", key, value));
		KTable<String, String> rightTable = streamsBuilder.table("right-stream",
				Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as("right-table-store"));
			rightTable.toStream().peek((key, value) -> log.info("Right incoming key is {} and value is {}", key, value));
		
		ValueJoiner<String, String, String> streamTableJoiner = (leftMessage, rightMessage) -> {
			return "Stream Message " + leftMessage + "is joined with Table Message " + rightMessage;
		};
		leftStream.join(rightTable, streamTableJoiner, Joined.with(Serdes.String(), Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Joined key: {} and value: {} ", key, value))
				.to("joined-topic", Produced.with(Serdes.String(), Serdes.String()));
		KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties.streamProperties());
		kafkaStreams.start();

	}

}
