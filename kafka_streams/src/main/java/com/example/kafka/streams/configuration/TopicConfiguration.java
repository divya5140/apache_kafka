package com.example.kafka.streams.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class TopicConfiguration {
	
	@Bean
	NewTopic inputTopic() {
		return TopicBuilder.name("kafka-stream-input")
				.partitions(2).build();
	}
	
	@Bean
	NewTopic outputTopic() {
		return TopicBuilder.name("kafka-stream-output")
				.partitions(2).build();
	}
	
	@Bean
	NewTopic inputTableTopic() {
		return TopicBuilder.name("kafka-table-input")
			.build();
	}
	
	@Bean
	NewTopic outputTableTopic() {
		return TopicBuilder.name("kafka-table-output")
				.build();
	}
	
	@Bean
	NewTopic leftStream() {
		return TopicBuilder.name("left-stream")
				.partitions(2).build();
	}
	@Bean
	NewTopic rightStream() {
		return TopicBuilder.name("right-stream")
				.partitions(2).build();
	}
	@Bean
	NewTopic joinedTopic() {
		return TopicBuilder.name("joined-topic")
				.partitions(2).build();
	}

	
}
