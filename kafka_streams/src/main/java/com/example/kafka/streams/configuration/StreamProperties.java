package com.example.kafka.streams.configuration;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.stereotype.Service;

@Service
public class StreamProperties {

public Properties streamProperties() {
		
		Properties streamProperties = new Properties();
		streamProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		streamProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "basic-streams");
		streamProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		streamProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		return streamProperties;
	}

}
