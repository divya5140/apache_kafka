package com.example.kafka.producer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaProducerConfig {

    @Bean
    NewTopic createTopic()
	{
		return new NewTopic("kafka",3,(short)1);
	}

}
