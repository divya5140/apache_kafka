package com.example.kafka.consumer.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaMessageListener {
	
	@KafkaListener(topics = "kafka",groupId = "kafka-group-1")
	public void consume1(String message)
	{
		log.info("message is consumed by user-consumer",message);
	}

	
}
