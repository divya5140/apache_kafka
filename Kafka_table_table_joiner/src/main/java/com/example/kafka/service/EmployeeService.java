package com.example.kafka.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.kafka.dto.Employee;
import com.example.kafka.dto.serializer.EmployeeSerializer;

@Service
public class EmployeeService {

	
	public void produce(String key,Employee employee ) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, EmployeeSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");


			
		
		try (KafkaProducer<String,Employee> producer = new KafkaProducer<>(properties)) {
			
			final ProducerRecord<String, Employee> producerRecord = new ProducerRecord<String,Employee>("employee", key, employee);
			producer.send(producerRecord);
		}
	}
}
