package com.example.kafka.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.kafka.dto.Department;
import com.example.kafka.dto.serializer.DepartmentSerializer;

@Service
public class DepartmentService {

	
public void produce(String key,Department department ) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, DepartmentSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");


			
		
		try (KafkaProducer<String,Department> producer = new KafkaProducer<>(properties)) {
			
			final ProducerRecord<String, Department> producerRecord = new ProducerRecord<String,Department>("department", key, department);
			producer.send(producerRecord);
		}
	}
}
