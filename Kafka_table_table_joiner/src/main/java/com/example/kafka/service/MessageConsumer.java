package com.example.kafka.service;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KTable;
import org.springframework.stereotype.Service;

import com.example.kafka.dto.Department;
import com.example.kafka.dto.Employee;
import com.example.kafka.dto.Office;
import com.example.kafka.dto.OfficeJoiner;
import com.example.kafka.serdes.DepartmentSerde;
import com.example.kafka.serdes.EmployeeSerde;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageConsumer {

	public void run() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "analytics-application");

		StreamsBuilder builder = new StreamsBuilder();

//		Serde<Department> departmentSerde = new DepartmentSerde();
//		Serde<Employee> employeeSerde = new EmployeeSerde();

		KTable<String, Department> department = builder.table("department",
				Consumed.with(Serdes.String(), new DepartmentSerde()));
		KTable<String, Employee> employee = builder.table("employee", 
				Consumed.with(Serdes.String(), new EmployeeSerde()));

		OfficeJoiner officeJoiner = new OfficeJoiner();

		KTable<String, Office> empDeptTable = employee.join(department, Employee::getDeptId, officeJoiner);

		empDeptTable.toStream().peek((key, value) ->
		log.info("key is :{} and value:{} ", key, value));

		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), properties);
		kafkaStreams.start();

	}
}
