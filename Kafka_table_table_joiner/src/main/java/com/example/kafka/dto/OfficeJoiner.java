package com.example.kafka.dto;

import org.apache.kafka.streams.kstream.ValueJoiner;

public class OfficeJoiner implements ValueJoiner<Employee, Department, Office> {

	@Override
	public Office apply(Employee employee, Department department) {
		 return Office.builder()
	        		
	        		.empName(employee.getEmpName())
	        		.empId(employee.getEmpId())
	        		.deptName(department.getDeptName())
	               
	                .build();
	}
}


