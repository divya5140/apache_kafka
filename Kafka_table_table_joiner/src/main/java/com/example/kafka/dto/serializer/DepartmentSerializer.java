package com.example.kafka.dto.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.example.kafka.dto.Department;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DepartmentSerializer implements Serializer<Department>{

	@Override
	public byte[] serialize(String topic, Department data) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			if(Objects.isNull(data)) {
				log.error("null value received at the serializer");
				return null;
			}
			return mapper.writeValueAsBytes(data);
		} catch (JsonProcessingException e) {
			throw new SerializationException("error occurred while serializing the data");
		}
	}

}

