package com.example.kafka.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.kafka.dto.Office;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OfficeDeserializer implements Deserializer<Office> {

	@Override
	public Office deserialize(String topic, byte[] data) {
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			if(Objects.isNull(data)) {
				log.error("null received at the deserializer");
				return null;
			}
			return mapper.readValue(data, Office.class);
		} catch (IOException e) {
			throw new SerializationException("error when deserializaion");
		}
			
		
	}
}
	
