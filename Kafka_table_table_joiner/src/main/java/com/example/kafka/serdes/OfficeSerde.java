package com.example.kafka.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.kafka.deserializer.OfficeDeserializer;
import com.example.kafka.dto.Office;
import com.example.kafka.dto.serializer.OfficeSerializer;

public class OfficeSerde extends Serdes.WrapperSerde<Office>{

	public OfficeSerde( ) {
		super(new OfficeSerializer(),new OfficeDeserializer());
	}
}
