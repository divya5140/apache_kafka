package com.example.kafka.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.kafka.deserializer.DepartmentDeserializer;
import com.example.kafka.dto.Department;
import com.example.kafka.dto.serializer.DepartmentSerializer;

public class DepartmentSerde extends Serdes.WrapperSerde<Department>{

	public DepartmentSerde( ) {
		super(new DepartmentSerializer(),new DepartmentDeserializer());
	}
}