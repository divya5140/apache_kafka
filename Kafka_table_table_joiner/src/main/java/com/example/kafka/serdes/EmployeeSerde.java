package com.example.kafka.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.kafka.deserializer.EmployeeDeserializer;
import com.example.kafka.dto.Employee;
import com.example.kafka.dto.serializer.EmployeeSerializer;

public class EmployeeSerde extends Serdes.WrapperSerde<Employee>{

	public EmployeeSerde( ) {
		super(new EmployeeSerializer(),new EmployeeDeserializer());
	}
}