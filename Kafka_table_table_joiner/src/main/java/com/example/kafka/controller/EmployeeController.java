package com.example.kafka.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.dto.Employee;
import com.example.kafka.service.EmployeeService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class EmployeeController {
	
	
	private final EmployeeService employeeService;
	
	@PostMapping("/employee")
	public void read(@RequestParam String key,@RequestBody Employee employee)
	{
		employeeService.produce(key, employee);
	}

}
