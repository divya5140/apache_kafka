package com.example.kafka.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.service.MessageConsumer;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {
	
	
	private final MessageConsumer messageConsumer;
	
	@GetMapping
	public void consume()
	{
		messageConsumer.run();
	}

}
