package com.example.kafka.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.kafka.dto.Department;
import com.example.kafka.dto.Employee;
import com.example.kafka.dto.Office;
import com.example.kafka.dto.OfficeJoiner;

@ExtendWith(SpringExtension.class)
public class OfficeJoinerTest {
	
	

	 	@Test
	    public void apply() {

	        Office office; 

	        Department department = Department.builder().deptId("d1").deptName("CSE").build();
	        	
	        Employee employee=Employee.builder().age(10).deptId("d1").empId("e1").empName("divya").build();
	        		
	        Office office2=Office.builder().deptName("CSE").empId("e1").empName("divya").build();	
	        		
	        
	        OfficeJoiner officeJoiner=new OfficeJoiner();
	        office=officeJoiner.apply(employee, department);
	        
	       
	        assertEquals(office, office2);
	    }
	
	
}
