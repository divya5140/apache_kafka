package com.example.kafka.stream.table;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamToTableApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamToTableApplication.class, args);
	}

}
