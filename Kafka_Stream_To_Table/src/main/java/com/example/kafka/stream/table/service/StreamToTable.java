package com.example.kafka.stream.table.service;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class StreamToTable {

	public Properties streamProperties() {

		Properties streamProperties = new Properties();
		streamProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		streamProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-to-table");
		streamProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		streamProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		return streamProperties;
	}

	public void StreamToTable() {

		StreamsBuilder builder = new StreamsBuilder();

		final KStream<String, String> stream = builder.stream("kafka-stream-to-table-input",
				Consumed.with(Serdes.String(), Serdes.String()));

		final KTable<String, String> convertedTable = stream.toTable(
				Materialized.<String, String, KeyValueStore<Bytes, byte[]>>as("stream-converted-to-table"));
				
			

		
		convertedTable.toStream().peek((key, value) -> log.info("Resultant key :{} and value :{}", key, value));

		
		convertedTable.toStream().to("kafka-stream-to-table-output", Produced.with(Serdes.String(), Serdes.String()));
		
		
		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), streamProperties());
		kafkaStreams.start();

	}

}
