package com.example.kafka.stream.table.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.stream.table.service.StreamToTable;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class StreamToTableController {

	private final StreamToTable streamToTable;
	
	@GetMapping("/stream-to-table")
	public void convertToTable()
	{
		streamToTable.StreamToTable();
	}
	
}
