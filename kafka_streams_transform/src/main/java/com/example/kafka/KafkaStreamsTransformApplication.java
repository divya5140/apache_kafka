package com.example.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamsTransformApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaStreamsTransformApplication.class, args);
	}

}
