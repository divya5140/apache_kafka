package com.example.kafka.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompositeKey {

	
	private String accountNumber;
	
	private LocalDate transactiondate;
}
