package com.example.kafka.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.dto.TransactionDto;
import com.example.kafka.service.ProducerService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class ProducerController {

	private final ProducerService producerService;

	@PostMapping("/kafka-stream-transform")
	public void produce(@RequestParam String key, @RequestBody TransactionDto transactionDto) {
		producerService.produce(key, transactionDto);
	}
}
