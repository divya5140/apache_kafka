package com.example.kafka.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.service.TransactionService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class TransactionController {

	
	
	private final TransactionService transactionService;
	
	@GetMapping("/transactions")
	public void run()
	{
		transactionService.run();
	}
}
