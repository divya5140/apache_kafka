package com.example.kafka.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.example.kafka.dto.TransactionDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaSerializer implements Serializer<TransactionDto>{

	@Override
	public byte[] serialize(String topic, TransactionDto data) {
	
		
		if(Objects.isNull(data))
			
		{
			log.error("Null value received at Serializer");
			return null;
			
		}
		ObjectMapper mapper=new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		
		try
		{
			return mapper.writeValueAsBytes(data);
		}
		catch (JsonProcessingException e) {
			throw new SerializationException("Error occured while Serializing");
		}
		
	}

}
