package com.example.kafka.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.kafka.deserializer.CompositeKeyDeserializer;
import com.example.kafka.dto.CompositeKey;
import com.example.kafka.serializer.CompositeKeySerializer;

public class CompositeKeySerde extends Serdes.WrapperSerde<CompositeKey>{

	public CompositeKeySerde() {
		super(new CompositeKeySerializer(),new CompositeKeyDeserializer());
		
	}

}
