package com.example.kafka.dto;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {

private int transactionId;

private String referenceNo;

private BigDecimal amount;

private String accountNumber;

private LocalDate transactionDate;

private String transactiontype;

private String transactionStatus;


}
