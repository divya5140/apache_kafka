package com.example.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaCompositeKeyApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaCompositeKeyApplication.class, args);
	}

}
