package com.example.kafka.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.kafka.dto.TransactionDto;
import com.example.kafka.serializer.KafkaSerializer;

@Service
public class ProducerService {

public void produce(String key,TransactionDto transactionDto ) {
		
	Properties properties = new Properties();
	properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
	properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaSerializer.class);
	properties.put(ProducerConfig.ACKS_CONFIG, "all");


		
	
	try (org.apache.kafka.clients.producer.KafkaProducer<String,TransactionDto> producer = new org.apache.kafka.clients.producer.KafkaProducer<>(properties)) {
		
		final ProducerRecord<String, TransactionDto> producerRecord = new ProducerRecord<String,TransactionDto>("transaction-analytics", key, transactionDto);
		producer.send(producerRecord);
	}
}
}