package com.example.kafka.service;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.stereotype.Service;

import com.example.kafka.dto.CompositeKey;
import com.example.kafka.dto.TransactionDto;
import com.example.kafka.serdes.CompositeKeySerde;
import com.example.kafka.serdes.TransactionSerde;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransactionService {
	
	private static Properties Operations() {
		
		Properties properties = new Properties();
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "analytics-application");
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, TransactionSerde.class);
		return properties;
	}
		
		
		public void run()
		{
		StreamsBuilder builder=new StreamsBuilder();
		Serde<TransactionDto> transactionSerde=new TransactionSerde();
		
		KStream<CompositeKey, TransactionDto> inputStream=builder.stream("transaction-analytics",Consumed.with(Serdes.String(),transactionSerde))
				.selectKey((key,value) -> new CompositeKey(value.getAccountNumber(),value.getTransactionDate()))
				.peek((key,value) -> log.info("Incoming key:{} and value:{} ",key.getAccountNumber(),value));
		
		
		inputStream.groupByKey(Grouped.with(new CompositeKeySerde(), new TransactionSerde()))
		.count().toStream().peek((key,value) -> log.info("Result key:{} and value:{} ",key,value));
		
		
		KafkaStreams kafkaStreams=new KafkaStreams(builder.build(), Operations());
		kafkaStreams.start();
		
	
		}

}



