package com.example.kafka.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.kafka.dto.TransactionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaDeserializer implements Deserializer<TransactionDto> {
	@Override
	public TransactionDto deserialize(String topic, byte[] data) {
	
	if(Objects.isNull(data))
		
	{
		log.error("Null value received at Deserializer");
		return null;
		
	}
	ObjectMapper mapper=new ObjectMapper();
	mapper.registerModule(new JavaTimeModule());
	
	try
	{
		return mapper.readValue(data, TransactionDto.class);
	}
	catch (IOException e) {
		throw new SerializationException("Error occured while Deserializing");
	}
	
	
	
}
}
