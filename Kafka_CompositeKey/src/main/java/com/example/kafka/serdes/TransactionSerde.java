package com.example.kafka.serdes;


import org.apache.kafka.common.serialization.Serdes;

import com.example.kafka.deserializer.KafkaDeserializer;
import com.example.kafka.dto.TransactionDto;
import com.example.kafka.serializer.KafkaSerializer;

public class TransactionSerde extends Serdes.WrapperSerde<TransactionDto>{

	public TransactionSerde( ) {
		super(new KafkaSerializer(),new KafkaDeserializer());
	}

}
