package com.example.kafka.aggregate.service;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Aggregator;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AggregateOperation {

	
	
private static Properties stateFulOperations() {
		
		Properties streamProperties = new Properties();
		streamProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		streamProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "basic-streams");
		streamProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,Serdes.String().getClass());
		streamProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Double().getClass());
		return streamProperties;
	}

public void executeAggegratorOperations() {
	
	StreamsBuilder builder = new StreamsBuilder();
	
	KStream<String,String> inputStream = builder
		.stream("stateful-operation", Consumed.with(Serdes.String(),Serdes.String()))
		.peek((key, value) -> log.info("Incoming stream key: {} and value:{}", key, value));
	
	Aggregator<String,String,Double> aggregator = (key, value, sum) -> Integer.parseInt(key)+ Integer.parseInt(value) + sum;
	
	inputStream.groupByKey().aggregate(()-> 0.0, aggregator, Materialized.with(Serdes.String() ,Serdes.Double()))
	.toStream().peek((key ,sum) -> log.info("Aggregated key:{} and sum:{} ", key ,sum))
	.to("stateful-output-topic", Produced.with(Serdes.String(), Serdes.Double()));;
	
;

	
	
	KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), stateFulOperations());
	kafkaStreams.start();
}


}


	

