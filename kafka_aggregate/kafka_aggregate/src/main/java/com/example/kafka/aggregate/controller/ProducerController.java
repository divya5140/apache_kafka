package com.example.kafka.aggregate.controller;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.aggregate.service.KafkaProducer;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
public class ProducerController {

	private final KafkaProducer kafkaProducer;
	
	@PostMapping("/aggregator")
	public void executeAggregatorOperation(@RequestParam String key,@RequestParam String value) {
		kafkaProducer.produce(key,value);
	}
}
