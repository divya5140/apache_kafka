package com.example.kafka.aggregate.service;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.Reducer;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReduceOperation {
	
private static Properties stateFulOperations() {
		
		Properties streamProperties = new Properties();
		streamProperties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		streamProperties.put(StreamsConfig.APPLICATION_ID_CONFIG, "basic-streams");
		streamProperties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG,Serdes.String().getClass());
		streamProperties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.Integer().getClass());
		return streamProperties;
	}
	public void reduceOperation()
	{
		
	
	
	StreamsBuilder builder=new StreamsBuilder();
	
	KStream<String, Integer> outputstream= builder
			.stream("reduce-operation",Consumed.with(Serdes.String(), Serdes.Integer()))
			.peek((key,value) ->log.info("Incoming key :{} and value:{}",key,value));
	
	Reducer<Integer> reducer= (valOne ,valTwo) -> valOne + valTwo ;
	
	outputstream.groupByKey().reduce(reducer,Materialized.with(Serdes.String(), Serdes.Integer()))
	.toStream().peek((key,value) -> log.info("reduce key :{} and value:{}" ,key,value))
	.to("reduce-output",Produced.with(Serdes.String(), Serdes.Integer()));

	KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), stateFulOperations());
	kafkaStreams.start();

	}
        

}
