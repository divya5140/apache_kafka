package com.example.kafka.aggregate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaAggregateApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaAggregateApplication.class, args);
	}

}
