package com.example.kafka.aggregate.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.aggregate.service.AggregateOperation;
import com.example.kafka.aggregate.service.ReduceOperation;

import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@RestController
public class AggregateController {

	private final AggregateOperation aggregateOperation;
	
	private final ReduceOperation reduceOperation;
	
	@GetMapping("/stateful-aggregator")
	public void executeAggregatorOperation() {
		aggregateOperation.executeAggegratorOperations();
	}

	
	@GetMapping("/stateful-reducer")
	public void executeReducerOperation() {
		reduceOperation.reduceOperation();
	}
}
