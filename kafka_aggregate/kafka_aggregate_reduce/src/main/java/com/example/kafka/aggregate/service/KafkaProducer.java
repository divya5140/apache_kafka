package com.example.kafka.aggregate.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.DoubleSerializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;


@Service
public class KafkaProducer {
@SuppressWarnings("unchecked")

public void produce(String key,Integer value ) {
		
		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		//properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
		
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		
		try (org.apache.kafka.clients.producer.KafkaProducer<String,Integer> producer = new org.apache.kafka.clients.producer.KafkaProducer<>(properties)) {
			
			final ProducerRecord<String, Integer> producerRecord = new ProducerRecord<String,Integer>("reduce-operation", key, value);
			producer.send(producerRecord);
		}
	}
	

}
