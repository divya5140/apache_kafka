package com.example.kafka.partitioning.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.partitioning.service.MessageReader;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {

	private final MessageReader messageReader;
	
	@GetMapping("/consume")
	public void consumeMessages() {
		messageReader.runConsumer();
	}
}



