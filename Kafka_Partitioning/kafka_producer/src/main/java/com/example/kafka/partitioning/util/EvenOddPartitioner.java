package com.example.kafka.partitioning.util;

import java.util.Map;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.common.Cluster;

public class EvenOddPartitioner implements Partitioner{
	
	private static final int EVEN_PARTITION = 0;
	private static final int ODD_PARTITION = 1;

	@Override
	public void configure(Map<String, ?> configs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
		
		String getPartition = extractPartition(value.toString());
		return "even".equals(getPartition) ? EVEN_PARTITION : ODD_PARTITION;
	}

	private String extractPartition(String number) {
		return (Integer.valueOf(number) % 2 == 0) ? "even" : "odd";
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

}
