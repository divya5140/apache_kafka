package com.example.kafka.serdes.service;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.kafka.serdes.dto.User;
import com.example.kafka.serdes.serializer.KafkaSerializer;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageProducer {

	public Future<RecordMetadata> produce(String key, User user) {

		Properties properties = new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaSerializer.class);
		try (KafkaProducer<String, User> producer = new KafkaProducer<>(properties)) {
			final ProducerRecord<String, User> producerRecord = new ProducerRecord<String, User>("kafka-serdes", key,
					user);
			return producer.send(producerRecord);
		}

	}

	public void printMetaData(final List<Future<RecordMetadata>> metadata, final String filePath) {
		log.info("Offset and partition of each data");
		metadata.forEach(data -> {
			try {
				final RecordMetadata recordMetadata = data.get();
				log.info("Record: {} written to partition: {} and offset: {} ", recordMetadata.partition(),
						recordMetadata.offset());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}

		});
	}

}
