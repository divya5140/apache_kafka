package com.example.kafka.consumer.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MessageReader {@KafkaListener(topics = "exception", groupId = "group-id")
public void consume(ConsumerRecord<String, Object> records) throws JsonMappingException, JsonProcessingException {

	

	log.info(records.value().toString()+ "");
}

}
