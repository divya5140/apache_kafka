package com.example.kafka.serdes.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.kafka.serdes.dto.User;
import com.example.kafka.serdes.serializer.JsonSerializer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageProducer {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(User user) {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, User> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());	
		
		log.info("Message Sent: "+user);
		System.out.println();
		ProducerRecord<String, User> record = new ProducerRecord<String, User>("exception", user);
		log.info(""+record);
		System.out.println(kafkaProducer.send(record));
		
 
		
	}
 
}
