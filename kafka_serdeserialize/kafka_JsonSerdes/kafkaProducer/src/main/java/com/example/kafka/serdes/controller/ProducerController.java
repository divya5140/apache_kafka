package com.example.kafka.serdes.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.kafka.serdes.dto.User;
import com.example.kafka.serdes.service.MessageProducer;

import lombok.RequiredArgsConstructor;


@RestController
@RequiredArgsConstructor
public class ProducerController {
	
	private final MessageProducer messageProducer;
	
	@PostMapping("/produce/events")
	public void sendUser(@RequestBody User user)
	{
		messageProducer.send(user);
	}

}
