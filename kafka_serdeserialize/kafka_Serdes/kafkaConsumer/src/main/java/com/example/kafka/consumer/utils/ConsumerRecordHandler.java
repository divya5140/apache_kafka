package com.example.kafka.consumer.utils;

import org.apache.kafka.clients.consumer.ConsumerRecords;

public interface ConsumerRecordHandler<K,V> {
	
	void process(ConsumerRecords<K,V> consumerRecords);

}
