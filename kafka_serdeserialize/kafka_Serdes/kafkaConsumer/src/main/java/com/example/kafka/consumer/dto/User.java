package com.example.kafka.consumer.dto;

import lombok.Data;

@Data
public class User {
	
	private int id;
	
	private String name;
	
	private String emailId;

}
