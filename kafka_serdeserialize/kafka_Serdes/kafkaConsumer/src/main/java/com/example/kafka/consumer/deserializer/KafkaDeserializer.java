package com.example.kafka.consumer.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.kafka.consumer.dto.User;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaDeserializer implements Deserializer<User>{

	@Override
	public User deserialize(String topic, byte[] data) {
		
		try
		{
			ObjectMapper mapper=new ObjectMapper();
			if(Objects.isNull(data))
			{
				log.error("Null received at deserializer");
				return null;
			}
			return mapper.readValue(data, User.class);
		}
		catch (IOException e) {
			throw new SerializationException("Error when deserilaization");
		}
		
	}

}
