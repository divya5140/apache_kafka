package com.example.kafka.serdes.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;

import com.example.kafka.serdes.dto.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaSerializer implements Serializer<User> {

	@Override
	public byte[] serialize(String topic, User data) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			if (Objects.isNull(data)) {
				log.error("Null value received at the serializer");
				return null;
			}
			return mapper.writeValueAsBytes(data);
		} catch (JsonProcessingException e) {
			throw new SerializationException("Error occured while serializing the data");
		}

	}

}
